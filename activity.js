db.rooms.insertOne({
    name: "single",
    accomodates: 2,
    price: 1000,
    description: "A simple room with all the basic necessities",
    roomsAvailable: 10,
    isAvailable: false
    })

db.rooms.insertMany([{
    name: "double",
    accomodates: 3,
    price: 2000,
    description: "A room fit for a small family going on a vacation",
    roomsAvailable: 5,
    isAvailable: false
    },
    {
      name: "double",
      accomodates: 3,
      description: "A room with a queen sized bed perfect for a simple getaway",
      roomsAvailable: 15,
      isAvailable: false   
    }
    ])

db.rooms.find({
    name: "double"
    })


//Nagdagdag po ako ng price maam, nakalimutan ko po kasi na ilagay pag insert ko kanina..
db.rooms.updateOne(
{roomsAvailable: 15},
{
    $set: {
        name: "queen",
        roomsAvailable: 0,
        price: 4000
    }
}



db.rooms.deleteMany({
    roomsAvailable: 0
    })
